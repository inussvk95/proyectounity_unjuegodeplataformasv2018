﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    GameObject text;
    private float lifetime = 0.3f;

    void Start()
    {
        text = GameObject.Find("Text");
        text.GetComponent<UnityEngine.UI.Text>().text = "Coins: " + Movimiento.Coins;
        StartCoroutine(Destructor());
    }

    IEnumerator Destructor ()
    {
        yield return new WaitForSeconds(lifetime);
        Movimiento.Coins++;
        if (Movimiento.Coins == 100)//Vida extra si coges 100 monedas
        {
            Movimiento.Coins = 0;
            Movimiento.Live++;
        }
        text.GetComponent<UnityEngine.UI.Text>().text = "Coins: " + Movimiento.Coins;//Contador de monedas
        Destroy(gameObject);
    }
}
