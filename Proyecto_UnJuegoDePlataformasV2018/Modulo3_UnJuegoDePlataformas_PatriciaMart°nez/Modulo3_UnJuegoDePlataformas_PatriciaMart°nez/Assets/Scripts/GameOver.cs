﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {

    public GameObject GameOverText;
    public static GameObject GameOverStatic;



	void Start ()
    {
        GameOver.GameOverStatic = GameOverText;
        GameOver.GameOverStatic.gameObject.SetActive(false); //Desactiva texto GameOver
    }
	
	public static void show()
    {
        GameOver.GameOverStatic.gameObject.SetActive(true); //Activa texto GameOver
    }



	void Update ()
    {
		
	}
}
