﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Items : MonoBehaviour
{

    private int Coins = 0;
    GameObject text;
    GameObject Mario;
    

    //Items que te puede dar un bloque
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Coin")
        {
            Destroy(collision.gameObject);
            Coins++;
            if(Coins == 100)
            {
                Coins = 0;
                Movimiento.Live++;
            }
            text.GetComponent<UnityEngine.UI.Text>().text = "Coins: " + Coins;
        }
        else if (collision.gameObject.tag == "seta")
        {
            Movimiento.Transformation = 2;
            Mario.GetComponent<Animator>().SetInteger("Transformation", Movimiento.Transformation);
            Mario.GetComponent<BoxCollider2D>().size = new Vector2(0.15f, 0.3f);
            Destroy(collision.gameObject);
        }
        else if(collision.gameObject.tag == "Estrella")
        {
            Movimiento.Invencible = true;
            Destroy(collision.gameObject);
        }
        else if(collision.gameObject.tag == "Flor")
        {
            Movimiento.Transformation = 3;
            Mario.GetComponent<Animator>().SetInteger("Transformation", Movimiento.Transformation);
            Mario.GetComponent<BoxCollider2D>().size = new Vector2(0.15f, 0.3f);
            Destroy(collision.gameObject);
        }
    }
  
    void Update()
    {
        if (Movimiento.Invencible) //Tiempo de invencibilidad de Mario al coger la estrella
        {
            Movimiento.TimeInvencible += Time.deltaTime;
        }
        if (Movimiento.TimeInvencible >= 5.0f)
        {
            Movimiento.Invencible = false;
            Movimiento.TimeInvencible = 0.0f;
        }
    }
    void Start ()
    {
        text = GameObject.Find("Text");
        Mario = GameObject.Find("Mario");
        text.GetComponent<UnityEngine.UI.Text>().text = "Coins: " + Coins; //Muestra por pantalla el numero de monedas
    }
}
