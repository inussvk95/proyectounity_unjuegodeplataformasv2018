﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YouWin : MonoBehaviour {

    //Variables
    public static YouWin instance = null;
    public GameObject youWinText;
  

    private void Awake() 
    {
        if(instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }


    }


    public void Win()
    {
        youWinText.SetActive(true);//Activa el texto 

      
    }
 

}
